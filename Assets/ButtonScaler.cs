﻿using UnityEngine;
using System.Collections;

public class ButtonScaler : MonoBehaviour {

    bool canStartScaleDown = false;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if( canStartScaleDown && gameObject)
        {

            gameObject.transform.localScale -= new Vector3(0.001f, 0.001f, 0.001f);
        }
    }

    public void OnStartScaleDownClicked()
    {
        canStartScaleDown = true;
    }
}

