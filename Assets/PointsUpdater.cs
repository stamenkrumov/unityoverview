﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointsUpdater : MonoBehaviour {

    public PlayerBehavior player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = player.Points.ToString();
	}
}
