﻿using UnityEngine;
using System.Collections;

public class PlayerBehavior : MonoBehaviour {
    [Range(0,300)]
    public float forceScale = 100f;
    public GameObject lostText;

    private bool isKeyDown = false;
    // Use this for initialization

    private int _points = 0;

    public int Points
    {
        get { return _points; }
    }

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        isKeyDown = false;

	    if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            isKeyDown = true; 
        }
        
	}

    void FixedUpdate()
    {
        if(isKeyDown)
            gameObject.GetComponent<Rigidbody>().AddForce(Vector3.forward * forceScale);
    }

    void OnCollisionEnter(Collision other)
    {
        print("Collision");
        if (other.gameObject.tag == "WinWall")
        {
            GameObject enviroment = GameObject.Find("Enviroment");
            Vector3 scale = enviroment.transform.localScale;

            enviroment.transform.position = new Vector3(enviroment.transform.position.x,
                                                        enviroment.transform.position.y,
                                                        enviroment.transform.position.z+3);
            enviroment.transform.Rotate(-1, 0, 0);
            gameObject.GetComponent<Rigidbody>().mass *= 2;
            print("You Won!");

            _points++;
        }
        else if(other.gameObject.tag == "LostWall")
        {
            print("You Lost!");
            lostText.SetActive(true);
        }
    }
}